package org.bitbucket.benner78.noughtsandcrosses;

import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import org.bitbucket.benner78.noughtsengine.Game;
import org.bitbucket.benner78.noughtsengine.Player;
import org.bitbucket.benner78.noughtsengine.UCT;

/**
 * Noughts and crosses game main activity class.
 */
public class MainActivity extends AppCompatActivity {

    private GameFragment gameFragment;
    final private String gameFragmentId = "game_fragment";
    final private Player FIRST_PLAYER = Player.NOUGHT;
    private Game game;
    private Player player;
    ComputerPlayer computerPlayer;

    /**
     * Class to asynchronously compute computer player's move.
     */
    private class ComputerPlayer extends AsyncTask<Game, Void, Game>
    {
        /**
         * Run the UCT algorithm.
         * @param game starting game state
         * @return new game state
         */
        @Override
        protected Game doInBackground(Game... game)
        {
            UCT uct = new UCT(new Game(game[0]));
            return uct.pickMoveUCT();
        }

        /**
         * Notify parent thread of result
         * @param game new game state.
         */
        @Override
        protected void onPostExecute(Game game)
        {
            computerPlayed(game);
        }
    }

    /**
     * Initialise activity
     * @param savedInstanceState saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button newGame = (Button)findViewById(R.id.newGame_button);
        newGame.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                initGame();
            }
        });

        // find the retained fragment on activity restarts
        FragmentManager fm = getSupportFragmentManager();
        gameFragment = (GameFragment) fm.findFragmentByTag(gameFragmentId);

        // create the fragment and data the first time
        if (gameFragment == null)
        {
            // add the fragment
            gameFragment = new GameFragment();
            fm.beginTransaction().add(gameFragment, gameFragmentId).commit();

            initGame();
            gameFragment.setGame(game);
        }
        else
        {
            game = gameFragment.getGame();
        }
    }

    /**
     * Save game state on activity being destroyed.
     */
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        // store the data in the fragment
        gameFragment.setGame(game);
    }

    /**
     * Start a new game.
     */
    private void initGame()
    {
        if (computerPlayer != null)
        {
            computerPlayer.cancel(true);
        }
        game = new Game(FIRST_PLAYER, getBoardSizeInput(), getLineSizeInput());
        this.player = getSelectedPlayerInput();
        invalidateBoard();

        if (player != FIRST_PLAYER)
        {
            computerPlay();
        }
    }

    /**
     * Get current game
     * @return current game
     */
    public Game getGame()
    {
        return game;
    }

    /**
     * Get human player
     * @return human player
     */
    public Player getPlayer()
    {
        return player;
    }

    /**
     * Let the computer take a move.
     */
    public void computerPlay()
    {
        if (!checkResult())
        {
            computerPlayer = new ComputerPlayer();
            computerPlayer.execute(game);
        }
    }

    /**
     * Redraw board.
     */
    public void invalidateBoard()
    {
        BoardView board = (BoardView)findViewById(R.id.boardView);
        board.invalidate();
    }

    /**
     * Method for the UCT thread to notify when it is finished.
     * @param game new game state
     */
    private void computerPlayed(Game game)
    {
        Log.d(getClass().getName(), "In computerPlayed()");

        if (this.game.getGameId() == game.getGameId())
        {
            this.game = game;
            invalidateBoard();
            checkResult();
        }
    }

    /**
     * Check if there is a winner
     * @return true or false
     */
    private boolean checkResult()
    {
        Log.d(getClass().getName(), "In checkResult()");
        Player winner = game.getWinner();
        if (winner != Player.NONE)
        {
            Toast.makeText(this, winner + " wins!", Toast.LENGTH_LONG).show();
            player = Player.NONE;
            return true;
        }
        else if (game.getLegalMoveCount() == 0)
        {
            Toast.makeText(this, winner + "Draw.", Toast.LENGTH_LONG).show();
            player = Player.NONE;
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Get human player's choice of nought or cross from UI
     * @return Player
     */
    private Player getSelectedPlayerInput()
    {
        RadioGroup playerRadioGroup = (RadioGroup)findViewById(R.id.player_radioGroup);
        int selected = playerRadioGroup.getCheckedRadioButtonId();
        if (selected == R.id.nought_radio)
        {
            return Player.NOUGHT;
        }
        else
        {
            return Player.CROSS;
        }
    }

    /**
     * Get board size from the UI
     * @return board size
     */
    private int getBoardSizeInput()
    {
        Spinner boardSpinner = (Spinner)findViewById(R.id.board_spinner);
        int size = Integer.parseInt((String)boardSpinner.getSelectedItem());
        Log.d(getClass().getName(), "BoardSize = " + size);
        return size;
    }

    /**
     * Get size of a winning line from the UI
     * @return line size
     */
    private int getLineSizeInput()
    {
        Spinner lineSpinner = (Spinner)findViewById(R.id.line_spinner);
        return Integer.parseInt((String)lineSpinner.getSelectedItem());
    }
}
