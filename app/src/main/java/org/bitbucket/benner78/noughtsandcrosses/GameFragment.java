package org.bitbucket.benner78.noughtsandcrosses;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import org.bitbucket.benner78.noughtsengine.Game;

/**
 * A {@link Fragment} subclass to store the game state.
 */
public class GameFragment extends Fragment {

    // data object we want to retain
    private Game game;

    // this method is only called once for this fragment
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // retain this fragment
        setRetainInstance(true);
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

}
