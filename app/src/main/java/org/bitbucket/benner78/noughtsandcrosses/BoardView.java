package org.bitbucket.benner78.noughtsandcrosses;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import org.bitbucket.benner78.noughtsengine.Game;
import org.bitbucket.benner78.noughtsengine.Player;
import org.bitbucket.benner78.noughtsengine.Point;

/**
 * Class to display noughts and crosses board.
 */
public class BoardView extends View {

    private Paint paint = new Paint();

    // Drawing dimensions
    private int contentWidth;
    private int contentHeight;
    private int size;
    private int offsetX;
    private int offsetY;
    private int lineWidth;
    private int pad;

    /**
     * Constructor.
     * @param context context
     */
    public BoardView(Context context)
    {
        super(context);
    }

    /**
     * Constructor.
     * @param context context
     * @param attrs attributes
     */
    public BoardView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    /**
     * Constructor.
     * @param context context
     * @param attrs attributes
     * @param defStyle style
     */
    public BoardView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    /**
     * Get which player is the human.
     * @return human player.
     */
    private Player getPlayer()
    {
        MainActivity mainActivity = (MainActivity)getActivity();
        if (mainActivity == null)
        {
            return Player.NONE;
        }
        return mainActivity.getPlayer();
    }

    /**
     * Notify the parent activity that the computer can play.
     */
    private void computerPlay()
    {
        MainActivity mainActivity = (MainActivity)getActivity();
        if (mainActivity == null)
        {
            return;
        }
        mainActivity.computerPlay();
    }

    /**
     * Get parent activity and strip any context wrappers.
     * @return parent activity
     */
    private Activity getActivity()
    {
        Context context = getContext();
        while (context instanceof ContextWrapper)
        {
            if (context instanceof Activity)
            {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }

    /**
     * Override of View.onLayout to set drawing dimensions as soon
     * as they are available.
     *
     * @param left Left position, relative to parent
     * @param top Top position, relative to parent
     * @param right Right position, relative to parent
     * @param bottom Bottom position, relative to parent
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        super.onLayout(changed, left, top, right, bottom);
        initDimensions();
    }

    /**
     * Set up view dimension fields used for drawing.
     */
    private void initDimensions()
    {
        contentWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        contentHeight = getHeight() - getPaddingTop() - getPaddingBottom();
        size = contentWidth < contentHeight ? contentWidth : contentHeight;
        offsetX = (contentWidth - size) / 2 + getPaddingLeft();
        offsetY = (contentHeight- size) / 2 + getPaddingTop();
        lineWidth = 8;
        pad = lineWidth * 8;
    }

    /**
     * Draw the board and pieces.
     * @param canvas canvas on which to draw.
     */
    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        MainActivity mainActivity = (MainActivity)getActivity();
        if (mainActivity == null)
        {
            return;
        }
        Game game = mainActivity.getGame();

        final int boardSize = game.getSize();
        final int spacing = size / boardSize;
        final int rectSize = spacing - (2 * pad);

        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(lineWidth);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);

        for (int i = 1; i < boardSize; ++i)
        {
            // Draw vertical lines
            canvas.drawLine(offsetX + spacing * i, offsetY,
                            offsetX + spacing * i, offsetY + size, paint);
            // Draw horizontal lines
            canvas.drawLine(offsetX, offsetY + spacing * i,
                            offsetX + size, offsetY + spacing * i, paint);
        }
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(lineWidth * 2);

        for (int row = 0; row < boardSize; row++)
        {
            for (int col = 0; col < boardSize; col++)
            {
                final int leftEdge = offsetX + pad + (col * spacing);
                final int rightEdge = leftEdge + rectSize;
                final int topEdge = offsetY + pad + (row * spacing);
                final int bottomEdge = topEdge + rectSize;

                final Point point = new Point(row, col);

                if (game.getPos(point) == Player.NOUGHT)
                {
                    // Draw noughts
                    canvas.drawCircle((rightEdge + leftEdge) / 2,
                                     (bottomEdge + topEdge) / 2,
                                     (rightEdge - leftEdge) / 2, paint);
                }
                else if (game.getPos(point) == Player.CROSS)
                {
                    // Draw crosses
                    canvas.drawLine(leftEdge, topEdge, rightEdge, bottomEdge, paint);
                    canvas.drawLine(leftEdge, bottomEdge, rightEdge, topEdge, paint);
                }
            }
        }
    }

    /**
     * Listen for touch events on game board.
     * @param motionEvent event
     * @return event consumed
     */
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent)
    {
        MainActivity mainActivity = (MainActivity)getActivity();

        if (mainActivity == null)
        {
            return true;
        }
        Game game = mainActivity.getGame();

        Log.d(getClass().getName(), "onTouch()");

        if (game.getTurn() != getPlayer())
        {
            return true;
        }

        final int boardSize = game.getSize();
        final int spacing = size / boardSize;
        final int rectSize = spacing - (2 * pad);

        for (int row = 0; row < boardSize; row++)
        {
            for (int col = 0; col < boardSize; col++)
            {
                final int leftEdge = offsetX + pad + (col * spacing);
                final int rightEdge = leftEdge + rectSize;
                final int topEdge = offsetY + pad + (row * spacing);
                final int bottomEdge = topEdge + rectSize;

                Rect r = new Rect(leftEdge, topEdge, rightEdge, bottomEdge);

                if (r.contains((int)motionEvent.getX(), (int)motionEvent.getY()))
                {
                    if (game.play(new Point(row, col)))
                    {
                        invalidate();
                        Log.d(getClass().getName(), "onTouch() played");
                        computerPlay();
                    }
                    return true;
                }
            }
        }
        return true;
    }

}
